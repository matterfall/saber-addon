execute at @s run summon minecraft:armor_stand ~ 0 ~ {Tags:["s_gen"],NoGravity:1,CustomName:'{"text":"generating"}',Invisible:1,Invulnerable:1,PersistanceRequired:1}
execute as @s if score Global s_auto_gen matches 0 run scoreboard players set Global s_auto_nether 1
scoreboard players set Global s_generated 1
tellraw @a ["",{"text":"Structures have started generating","color":"green"}]
playsound minecraft:block.note_block.bell master @a ~ ~ ~ 9 1