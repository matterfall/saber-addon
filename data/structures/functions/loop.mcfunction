#Loops every game tick

scoreboard players add clock s_clock 1
execute if score clock s_clock >= cMax s_clock run function structures:loop_functions
execute if score clock s_clock >= cMax s_clock run scoreboard players set clock s_clock 0

execute if score Global s_debug matches 1.. as @e[tag=s_structure] at @s run function structures:debug/debug
execute as @e[tag=s_structure,tag=s_kill] if score Global s_debug matches 1.. run tellraw @a ["",{"text":"Structure ID "},{"selector":"@s"},{"text":" spawned"}]
function structures:locate/water2
function structures:locate/lava2
function structures:locate/underwater2
function structures:locate/underlava2

function structures:generate/lifetime
function structures:y_range/structure_types_nether
function structures:generate/structure_types_nether
function structures:auto/nether
function structures:auto/end
function structures:safe/safe
#execute as @e[tag=s_structure,tag=nether_kill] if score Global s_debug matches 1.. run tellraw @a ["",{"text":"Structure ID "},{"selector":"@s"},{"text":" spawned"}]

execute as @e[tag=s_kill] at @s if block ~ ~ ~ structure_block run setblock ~ ~ ~ air
execute as @e[tag=s_kill] at @s if block ~ ~ ~1 redstone_block run setblock ~ ~ ~1 air
kill @e[tag=s_kill]