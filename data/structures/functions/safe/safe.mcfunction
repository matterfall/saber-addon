execute if score Global m_highlight matches 1.. run tag @e[type=minecraft:armor_stand,tag=s_safe] add s_highlight
execute unless score Global m_highlight matches 1.. run tag @e[type=minecraft:armor_stand,tag=s_safe] remove s_highlight
team leave @e[tag=s_safe,tag=!s_removal]

execute as @e[tag=s_safe] at @s run kill @e[tag=s_structure,distance=..200]

execute as @e[tag=s_safe,tag=s_highlight] run data merge entity @s {CustomNameVisible:1b}
execute as @e[tag=s_safe,tag=!s_highlight] run data merge entity @s {CustomNameVisible:0b}
effect give @e[tag=s_safe,tag=s_highlight] glowing 1 0 true
effect clear @e[tag=s_safe,tag=!s_highlight] glowing

execute at @e[tag=s_removal,tag=s_highlight] run particle minecraft:dust 1 0 0 1 ~ ~ ~ 0.3 0.3 0.3 0 3 force
execute as @e[tag=s_removal] at @s run team join s_remove @s