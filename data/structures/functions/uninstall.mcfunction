scoreboard objectives remove s_generated 
scoreboard objectives remove s_debug
scoreboard objectives remove m_highlight 
scoreboard objectives remove s_place 
scoreboard objectives remove s_density 
scoreboard objectives remove s_type 
scoreboard objectives remove s_clock 
scoreboard objectives remove s_lifetime 
scoreboard objectives remove s_auto_nether 
scoreboard objectives remove s_auto_end 
scoreboard objectives remove s_auto_gen
scoreboard objectives remove s_Y
scoreboard objectives remove s_Y
scoreboard objectives remove s_rot
scoreboard objectives remove s_posX
scoreboard objectives remove s_posZ
scoreboard objectives remove s_posVar

kill @e[type=minecraft:armor_stand,tag=s_gen]
kill @e[type=minecraft:armor_stand,tag=s_structure]
kill @e[type=minecraft:armor_stand,tag=s_safe]

team remove s_remove
tellraw @a ["",{"text":"[Structure Generator] by JDawgtor has been uninstalled.","color":"white"},{"text":"\nYou can now delete/disable the datapack.\n/reload will reinstall the datapack","color":"gray"}]