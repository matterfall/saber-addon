execute as @e[type=armor_stand,tag=!located,tag=s_structure,scores={s_type=86}] at @s run function structures:locate/surface
execute as @e[type=armor_stand,tag=located,tag=s_structure,scores={s_type=86}] at @s run tag @s add s_kill
execute as @e[type=armor_stand,tag=located,tag=s_structure,scores={s_type=86},tag=s_kill] at @s run setblock ~ ~ ~ minecraft:structure_block{ignoreEntities:0b,rotation:"NONE",posX:-3,posY:-2,posZ:-3,mode:"LOAD",name:"structures:ruined_structure"}
execute as @e[type=armor_stand,tag=located,tag=s_structure,scores={s_type=86},tag=s_kill] run function structures:generate/rotate
execute as @e[type=armor_stand,tag=located,tag=s_structure,scores={s_type=86},tag=s_kill] at @s run setblock ~ ~ ~1 minecraft:redstone_block

execute as @e[type=armor_stand,tag=!located,tag=s_structure,scores={s_type=206}] at @s run function structures:locate/surface
execute as @e[type=armor_stand,tag=located,tag=s_structure,scores={s_type=206}] at @s run tag @s add s_kill
execute as @e[type=armor_stand,tag=located,tag=s_structure,scores={s_type=206},tag=s_kill] at @s run setblock ~ ~ ~ minecraft:structure_block{ignoreEntities:0b,rotation:"NONE",posX:-16,posY:0,posZ:-16,mode:"LOAD",name:"structures:sol_base"}
execute as @e[type=armor_stand,tag=located,tag=s_structure,scores={s_type=206},tag=s_kill] run function structures:generate/rotate
execute as @e[type=armor_stand,tag=located,tag=s_structure,scores={s_type=206},tag=s_kill] at @s run setblock ~ ~ ~1 minecraft:redstone_block

