execute at @s unless block ~ ~ ~ minecraft:structure_block{rotation:"NONE"} run tag @s add no_spawn
execute if entity @s[tag=no_spawn] run tellraw @a [{"text":"Error: Structure ID ","color":"red"},{"score":{"name":"@s","objective":"s_type"}},{"text":"'s \"rotation\" was not set to NONE. Spawning canceled."}]
execute if entity @s[tag=no_spawn] run setblock ~ ~ ~ air
execute if entity @s[tag=no_spawn] run kill @s

execute at @s store result score @s s_rot run loot spawn ~ -200 ~ loot structures:rng/rotation


#90
execute if score @s s_rot matches 2 at @s store result score @s s_posX run data get block ~ ~ ~ posX
execute if score @s s_rot matches 2 run scoreboard players operation @s s_posVar = @s s_posX
execute if score @s s_rot matches 2 run scoreboard players operation @s s_posX -= @s s_posVar
execute if score @s s_rot matches 2 run scoreboard players operation @s s_posX -= @s s_posVar
execute if score @s s_rot matches 2 at @s store result block ~ ~ ~ posX int 1 run scoreboard players get @s s_posX

#180
execute if score @s s_rot matches 3 at @s store result score @s s_posX run data get block ~ ~ ~ posX
execute if score @s s_rot matches 3 run scoreboard players operation @s s_posVar = @s s_posX
execute if score @s s_rot matches 3 run scoreboard players operation @s s_posX -= @s s_posVar
execute if score @s s_rot matches 3 run scoreboard players operation @s s_posX -= @s s_posVar
execute if score @s s_rot matches 3 at @s store result block ~ ~ ~ posX int 1 run scoreboard players get @s s_posX

execute if score @s s_rot matches 3 at @s store result score @s s_posZ run data get block ~ ~ ~ posZ
execute if score @s s_rot matches 3 run scoreboard players operation @s s_posVar = @s s_posZ
execute if score @s s_rot matches 3 run scoreboard players operation @s s_posZ -= @s s_posVar
execute if score @s s_rot matches 3 run scoreboard players operation @s s_posZ -= @s s_posVar
execute if score @s s_rot matches 3 at @s store result block ~ ~ ~ posZ int 1 run scoreboard players get @s s_posZ

#270
execute if score @s s_rot matches 4 at @s store result score @s s_posZ run data get block ~ ~ ~ posZ
execute if score @s s_rot matches 4 run scoreboard players operation @s s_posVar = @s s_posZ
execute if score @s s_rot matches 4 run scoreboard players operation @s s_posZ -= @s s_posVar
execute if score @s s_rot matches 4 run scoreboard players operation @s s_posZ -= @s s_posVar
execute if score @s s_rot matches 4 at @s store result block ~ ~ ~ posZ int 1 run scoreboard players get @s s_posZ



#ROTATE
execute if score @s s_rot matches 1 at @s run data merge block ~ ~ ~ {rotation:"NONE"}
execute if score @s s_rot matches 2 at @s run data merge block ~ ~ ~ {rotation:"CLOCKWISE_90"}
execute if score @s s_rot matches 3 at @s run data merge block ~ ~ ~ {rotation:"CLOCKWISE_180"}
execute if score @s s_rot matches 4 at @s run data merge block ~ ~ ~ {rotation:"COUNTERCLOCKWISE_90"}