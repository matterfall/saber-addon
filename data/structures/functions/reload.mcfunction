scoreboard objectives add s_generated dummy

scoreboard objectives add s_debug dummy
scoreboard objectives add m_highlight dummy
scoreboard objectives add s_place dummy
scoreboard objectives add s_density dummy
scoreboard objectives add s_type dummy {"text":"Currently trying to spawn"}
scoreboard objectives add s_clock dummy
scoreboard objectives add s_lifetime dummy
scoreboard objectives add s_auto_nether dummy
scoreboard objectives add s_auto_end dummy
scoreboard objectives add s_auto_gen dummy
scoreboard objectives add s_Y dummy
scoreboard objectives add s_rot dummy
scoreboard objectives add s_posX dummy
scoreboard objectives add s_posZ dummy
scoreboard objectives add s_posVar dummy

gamerule commandBlockOutput false
execute unless score Density s_density matches 0.. run scoreboard players set Density s_density 2
execute unless score cMax s_clock matches 0.. run scoreboard players set Global s_auto_gen 1
execute unless score cMax s_clock matches 0.. run scoreboard players set cMax s_clock 10

team add s_remove
team modify s_remove color red
tellraw @a ["",{"text":"[Custom Structure Generator 1.3.0]","color":"lime"},{"text":" by JDawgtor loaded.\n","color":"white"},{"text":"Click here for help","underlined":true,"color":"yellow","clickEvent":{"action":"run_command","value":"/function generator:help"},"hoverEvent":{"action":"show_text","value":"Click for commands"}}]