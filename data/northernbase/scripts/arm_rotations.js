eventManager.on("setRotationAngles", function(e) {

	// check if entity is living entity 

		// get ability from the title set
		var ability = getAbility(e.getEntity(), 'saikou_arm');

		// if ability exists
		if (ability && ability.isEnabled()) {
			e.setRotationAngle('rightArm', 'x', -90);
			e.setRotationAngle('rightArm', 'y', 0);
			e.setRotationAngle('rightArm', 'z', 0);
		} 
	
});

function getAbility(entity, ability) {
    var abilities = entity.getAbilities();
    for (var i = 0; i < abilities.length; i++) {
        if (abilities[i].getId() === ability)
            return abilities[i];
    }
    return null;
}