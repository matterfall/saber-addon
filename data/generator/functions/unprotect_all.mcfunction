tag @e[tag=s_safe] remove s_removal
team leave @e[tag=s_safe]
execute at @s run tag @e[tag=s_safe] add s_removal
execute at @s as @e[tag=s_safe] run team join s_remove @s
execute if entity @e[tag=s_safe] run tellraw @s ["",{"text":"Remove all markers? "},{"text":"[Yes]","color":"green","clickEvent":{"action":"run_command","value":"/function structures:safe/remove_all"}},{"text":" "},{"text":"[No]","color":"red","clickEvent":{"action":"run_command","value":"/function structures:safe/keep"}}]
execute unless entity @e[tag=s_safe] run tellraw @s ["",{"text":"Can't find protection markers","color":"red"}]