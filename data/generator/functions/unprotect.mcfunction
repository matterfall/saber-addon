tag @e[tag=s_safe] remove s_removal
team leave @e[tag=s_safe]
execute at @s run tag @e[tag=s_safe,limit=1,sort=nearest] add s_removal
execute at @s as @e[tag=s_safe,limit=1,sort=nearest] run team join s_remove @s
execute if entity @e[tag=s_safe] run tellraw @s ["",{"text":"Remove nearest marker? "},{"text":"[Yes]","color":"green","clickEvent":{"action":"run_command","value":"/function structures:safe/remove"}},{"text":" "},{"text":"[No]","color":"red","clickEvent":{"action":"run_command","value":"/function structures:safe/keep"}}]
execute unless entity @e[tag=s_safe] run tellraw @s ["",{"text":"Can't find protection marker","color":"red"}]