scoreboard players add Global s_debug 1
execute if score Global s_debug matches 1 run scoreboard objectives setdisplay sidebar s_type
execute if score Global s_debug matches 1 run tellraw @a [{"text":"Debug mode is on"}]
execute if score Global s_debug matches 2.. run scoreboard objectives setdisplay sidebar
execute if score Global s_debug matches 2.. run tellraw @a [{"text":"Debug mode is off"}]
execute if score Global s_debug matches 2.. run scoreboard players set Global s_debug 0