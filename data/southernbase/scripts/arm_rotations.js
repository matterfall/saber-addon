eventManager.on("setRotationAngles", function(e) {

	// check if entity is living entity 

		// get ability from the title set
		var ability = getAbility(e.getEntity(), 'durendalarms');
		var ability2 = getAbility(e.getEntity(), 'durendalinsert');
		var ability3 = getAbility(e.getEntity(), 'durendalswordhold');
		var ability4 = getAbility(e.getEntity(), 'tridenthold');

		// if ability exists
		if (ability && ability.isEnabled()) {
			e.setRotationAngle('rightArm', 'x', -112.5);
			e.setRotationAngle('rightArm', 'y', -27.5);
			e.setRotationAngle('rightArm', 'z', -47.5); 
			e.setRotationAngle('leftArm', 'x', -37.5);
			e.setRotationAngle('leftArm', 'y', -27.5);
			e.setRotationAngle('leftArm', 'z', -27.5);
		}
		if (ability2 && ability2.isEnabled()) {
			e.setRotationAngle('rightArm', 'x', -27.5);
			e.setRotationAngle('rightArm', 'y', 27.5);
			e.setRotationAngle('rightArm', 'z', 0);
			e.setRotationAngle('leftArm', 'x', -45);
			e.setRotationAngle('leftArm', 'y', -30);
			e.setRotationAngle('leftArm', 'z', 0);
		}
		if (ability3 && ability3.isEnabled()) {
			e.setRotationAngle('rightArm', 'x', -82.5);
			e.setRotationAngle('rightArm', 'y', 0);
			e.setRotationAngle('rightArm', 'z', 90);
			e.setRotationAngle('leftArm', 'x', -90);
			e.setRotationAngle('leftArm', 'y', 0);
			e.setRotationAngle('leftArm', 'z', -15);
		}
		if (ability4 && ability4.isEnabled()) {
			e.setRotationAngle('rightArm', 'x', -90);
			e.setRotationAngle('rightArm', 'y', 0);
			e.setRotationAngle('rightArm', 'z', 0);
		}
	
});

function getAbility(entity, ability) {
    var abilities = entity.getAbilities();
    for (var i = 0; i < abilities.length; i++) {
        if (abilities[i].getId() === ability)
            return abilities[i];
    }
    return null;
}